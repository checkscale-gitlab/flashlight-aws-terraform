# flashlight-aws-terraform

Documentation
-------------
The idea of flashlight is to stop and start tagged EC2 instances by using HTTPS API calls:

- API Gateway v2 HTTP Endpoint
- Tagged EC2 instances
- cronjob for sending also a trigger event every evening at 19:00 (GMT), 20:00 (CET)

Cloudwatch Events Trigger for Stop
===================================

The EC2 machine is normally idling and not been used at night, so it's a good idea to stop the EC2 instances
(and the Aurora Serverless Database too). We will rise an event and stop the instance by cloudwatch calling
the lambda function.

Homepage
--------

https://gitlab.com/prunux/flashlight-aws-terraform


Icons
-----

<div>Icons made by <a href="https://www.flaticon.com/authors/catkuro" title="catkuro">catkuro</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

License
-------
    flashlight-aws-terraform is a set of System Description Language (HCL)
    files for Terraform (terraform.io) to run on AWS.
    Copyright (c) 2020, prunux.ch, Roman Plessl, All rights reserved.
    Copyright (c) 2020, Plessl + Burkhardt GmbH, All rights reserved.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


Copyright
---------

    Copyright (c) 2020, prunux.ch, Roman Plessl, All rights reserved.
    Copyright (c) 2020, Plessl + Burkhardt GmbH, All rights reserved.

    Part of this code was donated and paid by

    Stiftung 3FO, Belchenstrasse 7, CH-4600 Olten and
    Forem AG, Belchenstrasse 7, CH-4600 Olten

Authors
-------

* Roman Plessl
