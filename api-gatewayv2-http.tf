# API Gateway v2 HTTP
resource "aws_apigatewayv2_api" "flashlight-start" {
  name          = "flashlight-start"
  protocol_type = "HTTP"
  description   = "Flashlight Start"
  route_key     = "ANY /flashlight_start"
  target        = "arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:flashlight_start"
}

resource "aws_apigatewayv2_api" "flashlight-stop" {
  name          = "flashlight-stop"
  protocol_type = "HTTP"
  description   = "Flashlight Stop"
  route_key     = "ANY /flashlight_stop"
  target        = "arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:flashlight_stop"
}
