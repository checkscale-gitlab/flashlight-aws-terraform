## AWS Availability Zones and Region ##
# get all the available availability zones
data "aws_availability_zones" "available" {}

# default region by the webcall
data "aws_region" "current" {}

# get some AWS metadata about our terraform-user account
data "aws_caller_identity" "current" {}

### Ubuntu AMIs
## get lastest ubuntu 18.04 amd64
data "aws_ami" "ubuntu-bionic-amd64" {
  most_recent = "true"

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # canonical
}

# ## encrypt ubuntu 18.04 amd64
# resource "aws_ami_copy" "ubuntu-bionic-amd64-encrypted" {
#   name              = "ubuntu-bionic-amd64-encrypted"
#   description       = "An encrypted root ami based off ${data.aws_ami.ubuntu-bionic-amd64.id}"
#   source_ami_id     = data.aws_ami.ubuntu-bionic-amd64.id
#   source_ami_region = data.aws_region.current
#   encrypted         = true

#   tags = {
#     ImageType = "ubuntu-bionic-amd64-encrypted"
#     Name      = "ubuntu-bionic-amd64-encrypted"
#     Terraform = "true"
#   }
# }

## get lastest ubuntu 18.04 arm64
data "aws_ami" "ubuntu-bionic-arm64" {
  most_recent = "true"

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-arm64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # canonical
}

# resource "aws_ami_copy" "ubuntu-bionic-armd64-encrypted" {
#   name              = "ubuntu-bionic-arm64-encrypted"
#   description       = "An encrypted root ami based off ${data.aws_ami.ubuntu-bionic-arm64.id}"
#   source_ami_id     = data.aws_ami.ubuntu-bionic-arm64.id
#   source_ami_region = data.aws_region.current
#   encrypted         = true

#   tags = {
#     ImageType = "ubuntu-bionic-arm64-encrypted"
#     Name      = "ubuntu-bionic-arm64-encrypted"
#     Terraform = "true"
#   }
# }
