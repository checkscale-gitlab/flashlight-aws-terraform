import json
import boto3

# Enter the region your instances are in. Include only the region without specifying Availability Zone; e.g., 'us-east-1'
region = 'eu-west-1'

def lambda_handler(event, context):
    ec2 = boto3.resource('ec2', region_name=region)
    filters = [{ 'Name': 'tag:Flashlight', 'Values': ['enabled'] }]
    status = ec2.instances.filter(Filters = filters).stop()
    return {
        'statusCode': 200,
        'body': 'Stopped Flashlight Instances!\n\n'+'### STATUS ###\n\n'+json.dumps(status, indent=4, sort_keys=False)
    }
